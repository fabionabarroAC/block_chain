import blockChain.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Miner implements Runnable{
    private static final Logger logger = LoggerFactory.getLogger(Miner.class);
    private static List<Block> blockchain = new ArrayList<>();
    private final String data;

    public Miner(String data) {
        this.data = data;
        var miner = new Thread(this);
        miner.start();
    }

    public void run(){
        if (!data.equals("exit")){
            var newBlock = new Block(data, getPreviousHashFromLastBlock(), new Date().getTime());
            int prefix = 3;
            newBlock.mineBlock(prefix);
            blockchain.add(newBlock);
            logger.info(String.format("The data %s was mined with hash '%s' ", data, newBlock.getHash()));
        }
        else{
        logger.info(String.format("The blocks at Blockchain %s", blockchain.toString()));;
        }
    }

    public String getPreviousHashFromLastBlock(){
        return blockchain.stream().reduce((a , b) -> b).orElse(genesisBlock()).getHash();
    }

    public Block genesisBlock(){
        return new Block("This is the Genesis Block.", "0", new Date().getTime());
    }

}