import java.util.Scanner;

public class Application {
    public static String dataConsole;
    public static void main(String[] args) throws InterruptedException {
        var scanner = new Scanner(System.in);
        do {
            System.out.println("Please insert new data to mine or 'exit' to exit ");
            dataConsole = scanner.nextLine();
            var miner = new Miner(dataConsole);
        }while(!dataConsole.equals("exit"));
    }
}
